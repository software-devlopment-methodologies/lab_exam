const express = require('express')
const db = require('./db')

const app = express();
app.use(express.json())

app.get("/", (request, response)=>{
    const connection = db.openConnection();
    const sql = `SELECT * FROM book`
    connection.query(sql, (error, data)=>{
      //  connection.end()
        response.send(data);
    })
})

app.post("/", (request, response)=>{
    const connection = db.openConnection();
    const {book_id, book_title, publisher_name, author_name} = request.body
    const sql = `INSERT INTO book VALUES(${book_id}, '${book_title}', ${publisher_name}, ${author_name})`
     connection.query(sql, (error, data)=>{
     //  connection.end()
        response.send(data)
    })
})

app.put("/:book_id", (request, response)=>{
    const connection = db.openConnection();
    const { book_id } = request.params
    const {publisher_name,author_name} = request.body
    const sql = `UPDATE book SET publisher_name = '${publisher_name}', author_name = ${author_name} WHERE book_id = ${book_id}`

    connection.query(sql, (error, data)=>{
     //   connection.end()
        response.send(data)
    })
})

app.delete("/:book_id", (request, response)=>{
    const connection = db.openConnection();
    const { book_id } = request.params
   
    const sql = `DELETE FROM book WHERE book_id = ${book_id}`

    connection.query(sql, (error, data)=>{
    //    connection.end()
        response.send(data)
    })
})

app.listen(4000, ()=>{

    console.log("Backend listening at port 4000...!!!")
})

