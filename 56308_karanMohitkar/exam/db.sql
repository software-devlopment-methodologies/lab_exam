create table book (
  book_id integer primary key auto_increment,
  book_title VARCHAR(200),
  publisher_name VARCHAR(200),
  author_name VARCHAR(200)
);

INSERT INTO book VALUES(1, "wings of fire","Abc","xyz");
INSERT INTO book VALUES(2, "death clutch","benjamin","brock");
INSERT INTO book VALUES(3, "curtosy Hall","sydmun","taco");
INSERT INTO book VALUES(4, "fisherman ","chekus","el Sharaway");


